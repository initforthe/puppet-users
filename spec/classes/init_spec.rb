# frozen_string_literal: true

require 'spec_helper'

describe 'users' do
  on_supported_os.each do |os, os_facts|
    let(:facts) { os_facts }

    context "on #{os}" do
      context 'with default values for all parameters' do
        it { is_expected.to compile }
      end

      context 'with a simple account' do
        let(:params) do
          {
            accounts: {
              'simmerz' => {},
            },
          }
        end

        it { is_expected.to compile }
        it { is_expected.to contain_accounts__user('simmerz') }
      end
    end
  end
end
