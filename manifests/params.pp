# Add something intelligent
# @param accounts [Hash] Hash of users to manage
# @param ignore_password_if_empty [Boolean] Ignore password if empty
class users::params (
  Hash $accounts                    = {},
  Boolean $ignore_password_if_empty = true,
) {
}
