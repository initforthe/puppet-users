# Users and group management
# @param accounts [Hash] Hash of users to manage
# @param ignore_password_if_empty [Boolean] Ignore password if empty
class users (
  Hash $accounts                    = $users::params::accounts,
  Boolean $ignore_password_if_empty = $users::params::ignore_password_if_empty,
) inherits users::params {
  if $accounts {
    $accounts.each |$username, $userdata| {
      $ssh_key = lookup("users::${username}::ssh_key", undef, undef, undef)
      $password = lookup("users::${username}::password", undef, undef, '')
      $user_ensure = $userdata['ensure'] ? {
        'absent' => absent,
        default  => present,
      }

      $install_ssh_key = $userdata['install_ssh_key'] ? {
        true    => true,
        default => false,
      }
      $options = delete($userdata, 'install_ssh_key')

      if $ssh_key {
        $ssh_key_option = { 'sshkeys' => [$ssh_key] }
      } else {
        $ssh_key_option = {}
      }

      $user_options = $options + $ssh_key_option

      accounts::user { $username:
        *                        => $user_options,
        password                 => $password,
        ignore_password_if_empty => $ignore_password_if_empty,
        notify                   => Exec["clear_password_${username}"],
      }

      if ($ssh_key) and ($install_ssh_key) and ($user_ensure == 'present') {
        accounts::key_management { "${username}_key_management":
          ensure       => present,
          user         => $username,
          group        => $username,
          sshkeys      => [$ssh_key],
          sshkey_owner => $username,
          sshkey_group => $username,
          user_home    => $userdata['home'],
        }
      }

      $clear_password_command = $userdata['ensure'] ? {
        'absent' => '/bin/true',
        default  => "/usr/bin/passwd -d ${username}",
      }

      exec { "clear_password_${username}":
        command => $clear_password_command,
        unless  => "/bin/grep ${username} /etc/shadow | /usr/bin/awk -F \":\" '(\$2 == \"!\" || \$2 == \"!!\")' | exit \$(wc -l)",
      }
    }
  }
}
